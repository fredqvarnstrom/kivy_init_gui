# -*- coding: utf-8 -*-

import glob

import os

import sys
from kivy.app import App
from kivy.lang import Builder

from kivy.properties import StringProperty, BooleanProperty, ListProperty, ObjectProperty
from kivy.uix.screenmanager import Screen
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import SlideTransition


class CautionPopup(Popup):
    pass


class ConfirmPopup(Popup):
    pass


class MainScreen(Screen):
    fullscreen = BooleanProperty(False)

    def add_widget(self, *args):
        if 'content' in self.ids:
            return self.ids.content.add_widget(*args)
        return super(MainScreen, self).add_widget(*args)


class MainApp(App):
    current_title = StringProperty()        # Store title of current screen
    screen_names = ListProperty([])
    screens = {}                            # Dict of all screens
    hierarchy = ListProperty([])

    # Popup instances
    caution_popup = ObjectProperty(None)
    confirm_popup = ObjectProperty(None)

    def build(self):
        """
        base function of kivy app
        :return:
        """

        self.load_screen()

        self.caution_popup = CautionPopup()
        self.confirm_popup = ConfirmPopup()

        self.go_screen('entry', 'right')

    def confirm_yes(self):
        """
        callback function of "Yes" button in confirm popup
        :return:
        """
        self.confirm_popup.dismiss()

    def confirm_no(self):
        """
        callback function of "No" button in confirm popup
        :return:
        """
        self.confirm_popup.dismiss()

    def go_screen(self, dest_screen, direction):
        """
        Go to given screen
        :param dest_screen:     destination screen name
        :param direction:       "up", "down", "right", "left"
        :return:
        """
        sm = self.root.ids.sm

        sm.transition = SlideTransition()
        screen = self.screens[dest_screen]
        sm.switch_to(screen, direction=direction)
        self.current_title = screen.name

    def load_screen(self):
        """
        Load all screens from data/screens to Screen Manager
        :return:
        """
        available_screens = []

        full_path_screens = glob.glob("screens/*.kv")

        for file_path in full_path_screens:
            file_name = os.path.basename(file_path)
            available_screens.append(file_name.split(".")[0])

        self.screen_names = available_screens
        for i in range(len(full_path_screens)):
            screen = Builder.load_file(full_path_screens[i])
            self.screens[available_screens[i]] = screen
        return True


if __name__ == '__main__':

    app = MainApp()
    app.run()
